# Git Tools for BM
## pre-auth
### Description
Create pull request to bitbucket when pushing.
### Installation
You have to modify the file to set credential and repository.
* AUTH : username:password
* PR_TO : fork(s repository
```
AUTH="cmarneux:***"
PR_TO="cmarneux/git-tools"
```
You have to put this file in your local repository and make it executable :
```
chmod u+x ${GIT_REPOSITORY}/.git/hooks/pre-push
```
## gitconfig
* Add name and email to identify commit
* filter tabspace replace tab by 4 spaces for local edit and replace 4 spaces by tab when committing
* Set global gitignore file
## git folder
### attributes
Defined files formats for displaying malformed files before commit
### gitignore
Set global files to ignore in all repositories
